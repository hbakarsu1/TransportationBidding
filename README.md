# Transportation Bidding

Hello Uber Team! Below you can see the screenshots of my Android application and detailed explanation of each screen.

![Screenshot_2019-06-13-01-10-21-061_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415986-3942c500-8dcd-11e9-9b1e-9ad2f8a741b1.png)
![Screenshot_2019-06-13-01-10-44-102_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415987-39db5b80-8dcd-11e9-8f1f-e4e4ba019f6d.png)

The screen that appears when the application is first opened will be the login screen. If you already have an account, you can log in from here. If you do not have an account, you will be directed to the registration page by clicking the "Sign Up" button. After filling out all the required information on the registration page, you can become a member of the application.


![Screenshot_2019-06-13-01-11-11-665_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415988-39db5b80-8dcd-11e9-8d97-e775da274279.png)
![Screenshot_2019-06-13-01-11-37-135_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415990-3a73f200-8dcd-11e9-9567-246ebaa6fae1.png)

After logging in, you will encounter one of the two pages seen at the top. The image on the right represents the main page for normal users, while the one on the left represents the main page for administrators. These pages contain job offers, and users are expected to click on the jobs they have chosen for themselves. When normal users click on job offers on the main page, they also encounter job details, as seen in the image below. In the job details, users can see the number of bids made for this job and the bid amounts.


![Screenshot_2019-06-13-01-12-25-299_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415991-3b0c8880-8dcd-11e9-9584-450de6eee198.png)
![Screenshot_2019-06-13-01-12-42-169_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415992-3b0c8880-8dcd-11e9-8a1e-f20bd753c177.png)

The section below contains documents and forms that users need to fill out before applying for jobs. Personal information, commercial details, bank account information, and documents should be completed accurately. Otherwise, the account will not be verified, and the bids made for jobs will be disregarded.


![Screenshot_2019-06-13-01-12-49-239_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415994-3ba51f00-8dcd-11e9-9a1c-76a1711cf037.png)
![Screenshot_2019-06-13-01-12-54-252_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415996-3ba51f00-8dcd-11e9-8b20-05590209d011.png)
![Screenshot_2019-06-13-01-12-59-654_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59415999-3c3db580-8dcd-11e9-85a4-45e142d1c8ef.png)
![Screenshot_2019-06-13-01-13-04-668_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416000-3cd64c00-8dcd-11e9-9aaf-6496af9fdec3.png)


Here, you can see the details of the selected ID card, SRC Certificate, Driver's License, and Psychotechnical document pages. In this section, you should either upload images for the requested documents or fill in the information accurately in the sections where information needs to be provided.


![Screenshot_2019-06-13-01-13-07-986_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416001-3cd64c00-8dcd-11e9-851e-b654e82c0b7a.png)
![Screenshot_2019-06-13-01-13-21-215_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416002-3cd64c00-8dcd-11e9-8863-ee10c947168d.png)
![Screenshot_2019-06-13-01-13-24-230_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416004-3d6ee280-8dcd-11e9-8b5c-11401011b3b8.png)
![Screenshot_2019-06-13-01-13-27-136_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416005-3d6ee280-8dcd-11e9-95bb-f19fb625b3b0.png)


![Screenshot_2019-06-13-01-13-33-381_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416006-3e077900-8dcd-11e9-846a-0ace8b1c26d8.png)
![Screenshot_2019-06-13-01-13-47-896_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416010-3ea00f80-8dcd-11e9-916e-1e9d3aa2544b.png)


If you edit a document or information, when it is successfully uploaded to the system, you will see a confirmation message as shown in the image above.


![Screenshot_2019-06-13-01-13-43-706_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416009-3e077900-8dcd-11e9-96c4-aa6305155963.png)


As seen in the image, the basic navigation of the application is done through the Navigation Menu on the left. From here, you can access all pages and complete any missing information.


![Screenshot_2019-06-13-01-13-58-503_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416012-3ea00f80-8dcd-11e9-93fd-1181c2875c77.png)


If you want to change your password, you can easily update it by accessing the password change page.


![Screenshot_2019-06-13-01-14-27-281_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416014-3f38a600-8dcd-11e9-9ca3-d00b427b54b1.png)
![Screenshot_2019-06-13-01-14-34-678_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416015-3fd13c80-8dcd-11e9-9853-0b8e2e853840.png)
![Screenshot_2019-06-13-01-14-52-017_com muhammedsamilozer tasimacilik](https://user-images.githubusercontent.com/32862255/59416016-3fd13c80-8dcd-11e9-9660-2a0cd7c91b08.png)


In the last section shown, there are detailed pages for the admin to edit and delete jobs, access detailed information about users, and view payment pages for completed or pending jobs.

The system is built using the Volley library for Android and a combination of MySQL. All entered information is stored in the database. To facilitate user login and provide a flexible design, the "Session" logic has been integrated into the application. Thanks to sessions, when you close and reopen the application, your password will be remembered, and you will be redirected to the main page without having to log in again.

Hayri Bora Akarsu
